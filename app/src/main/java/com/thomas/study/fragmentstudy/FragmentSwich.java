package com.thomas.study.fragmentstudy;

/**
 * Created by thomas on 2017-09-03.
 */

public interface FragmentSwich {
    void swichToFragment1();
    void swichToFragment2();
    void swichToFragment3();
    void swichToFragment4();
    void swichToFragment1_1();
    void swichToFragment1_2();

}
