package com.thomas.study.fragmentstudy;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.TooManyListenersException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment4 extends Fragment {
    public static final int REQUEST_CODE=0;

    @BindView(R.id.f4_tv1) TextView f4Tv1;
    Unbinder unbinder;
    String abc;
    public Fragment4() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment4, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.f4_btn1)
    public void onViewClicked() {
/*        Fragment3 f3 = new Fragment3();
        f3.setOnMessageReceiveListener(new Fragment3.onMessageReceiveListener() {
            @Override
            public void onReceived(String message) {
                Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
            }
        });*/

/*        Intent intent = new Intent(getActivity(),MainActivity2.class);
        startActivity(intent);*/

        Intent intent = new Intent(getContext(),MainActivity2.class);
        intent.putExtra("name","omg");
        intent.putExtra("age",12);
        startActivityForResult(intent,REQUEST_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE && resultCode == Activity.RESULT_OK){
            String result = data.getStringExtra(MainActivity2.RESULT_MESSAGE);
            Toast.makeText(getActivity(),result,Toast.LENGTH_SHORT).show();
        }
    }
}
