package com.thomas.study.fragmentstudy;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1_1 extends Fragment {
    FragmentSwich fragmentSwich;
    @BindView(R.id.f1_1tv1) TextView f11tv1;
    @BindView(R.id.f1_1tv2) TextView f11tv2;

    public interface ResultListener{
        public void sendResult(int value);
    }
    ResultListener resultListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment1_1, container, false);
        ButterKnife.bind(this, v);

        if(resultListener !=null) {
            resultListener.sendResult(10);
        }
/*

        getActivity();//fragment가 속한 Activitiy를 가져옴
        getContext();//fragment가 속한 Context를 가져옴
        getArguments();setArguments();//Fragment에 Argument를 설정하거나 얻어옴
        getFragmentManager();//자신이 속한 FragmentManager를 얻어옴
        getChildFragmentManager();//fragment내에 fragmentf를 배치할 수 있는 fragmentManager를 얻어옴
        getParentFragment();//ChildFragmentManager에 추가된 경우 ParentFragment를 얻는다
        getLoaderManager();//Activitiy의; LoaderManager를 가져옴
        getView(); //onCreateView에서 설정된 View를 가져옴
        startActivity(); //activitiy 구동
        startActivityForResult();//activitiy 구동 and결과값
        setHasOptionsMenu();//Fragment의 메뉴를 생성하거나 처리한다
        onCreateOptionsMenu();//Fragment의 메뉴를 생성하거나 처리한다
        onOptionsItemSelected();//Fragment의 메뉴를 생성하거나 처리한다
        onCreateAnimation()//fragment에서 사용할 Animation을 생성해 넘겨준다

*/
        //activitiy에서 받은 데이터값 불러오기
        Bundle b = getArguments();
        if(b !=null) {
            String name = b.getString("name");
            int age = b.getInt("age");
        f11tv1.setText(name);
        f11tv2.setText(String.valueOf(age));
        }
        return v;
    }

    @OnClick(R.id.f1_1btn1)
    public void onViewClicked() {
        fragmentSwich.swichToFragment1_2();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ResultListener){
            resultListener =(ResultListener)context;
        }else{
            Log.e("dd","not resultlistener");
        }
        this.fragmentSwich = (FragmentSwich) context;
    }
}
