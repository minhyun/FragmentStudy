package com.thomas.study.fragmentstudy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity2 extends AppCompatActivity {
    public static final String RESULT_MESSAGE="result";
    @BindView(R.id.activity2_tv1)
    TextView activity2Tv1;
    @BindView(R.id.activity2_tv2)
    TextView activity2Tv2;

    String name;
    int age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        Intent e = getIntent();
        String name = e.getStringExtra("name");
        int age = e.getIntExtra("age", 0);

        activity2Tv1.setText(name);
        activity2Tv2.setText(String.valueOf(age));



    }

    @OnClick(R.id.activity2_btn1)
    public void onViewClicked() {
        Intent data = new Intent();
        data.putExtra(RESULT_MESSAGE, name + String.valueOf(age));
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}
