package com.thomas.study.fragmentstudy;


import android.content.Context;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment {
    private final static String F1TAG="tag1";
    FragmentSwich fragmentSwich;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_fragment1, container, false);
        ButterKnife.bind(this,v);

        return v;
    }
    @OnClick(R.id.f1_btn1)void clickin(){
        fragmentSwich.swichToFragment1_1();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.fragmentSwich=(FragmentSwich)context;
    }
}
