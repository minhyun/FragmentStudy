package com.thomas.study.fragmentstudy;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment3 extends Fragment {

    Unbinder unbinder;

/*    //값전달 위한 리스너 fragment4에 보내자
    public interface onMessageReceiveListener {
        public void onReceived(String message);
    }

    onMessageReceiveListener mListener;
    public void setOnMessageReceiveListener(onMessageReceiveListener listener) {
        mListener = listener;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment3, container, false);

/*        if(mListener != null){
            mListener.onReceived("야호~~~~~~~");
        }*/

        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
