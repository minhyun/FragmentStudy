package com.thomas.study.fragmentstudy;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {


    public Fragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //다른액티비티에서는 값전달 못하도록 체크
        if(((MainActivity)getActivity()) !=null && ((MainActivity)getActivity()) instanceof MainActivity) {
            ((MainActivity) getActivity()).mData("yaaaaaa");
            ((MainActivity) getActivity()).abc="omg";
        }
        return inflater.inflate(R.layout.fragment_fragment2, container, false);
    }

}
