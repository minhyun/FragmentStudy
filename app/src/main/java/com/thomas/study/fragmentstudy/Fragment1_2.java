package com.thomas.study.fragmentstudy;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Fragment1_2 extends Fragment {

    //fragment가 fragmentManager에 추가되었을때 호출된다
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    //fragment 생성시 초기화 처리를 하도록 호출된다
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //fragment에 배치할 view를 생성해 달라고 요청하기 위해 호출 fragment에 배치할 view를 생성하여 넘겨준다
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment1_2, container, false);
    }

    //fragment에 배치할 View가 생성되었을 때 호출한다
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //Activitiy의 onCreate가 완료된 다음 호출된다. fragment의 View는 Activitiy에 추가되어 있는 상태가 된다
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //View 의 상태 저장값에 대한 복원이 완료되면 호출
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    //fragment가 사용자에게 보여졌을 때 호출
    @Override
    public void onStart() {
        super.onStart();
    }

    //fragment가 사용자에게 보여지고 Activitiy가 running상태이면 호출
    @Override
    public void onResume() {
        super.onResume();
    }

    //fragment가 더이상 resumed상태가 아닐때 호출
    @Override
    public void onPause() {
        super.onPause();
    }

    //fragment가 더이상 started 상태가 아닐때 호출
    @Override
    public void onStop() {
        super.onStop();
    }

    //fragment에서 onCreateView로 생성한 View를 제거할 떄 호출
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    //Fragment가 더 이상 사용되지 않을때 호출
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //fragment가 fragmentManager에서 제거 될떄 호출
    @Override
    public void onDetach() {
        super.onDetach();
    }
}
