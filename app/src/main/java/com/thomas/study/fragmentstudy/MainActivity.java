package com.thomas.study.fragmentstudy;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements Fragment1_1.ResultListener,FragmentSwich{
    String abc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //갑작스런 앱종료후 자동실행시 이중 생성방지
        if(savedInstanceState ==null) {
            swichToFragment1();
        }
    }
    @OnClick({R.id.main_btn1,R.id.main_btn2,R.id.main_btn3,R.id.main_btn4}) void click(View v) {
        switch (v.getId()) {
            case R.id.main_btn1:
                swichToFragment1();
                break;
            case R.id.main_btn2:
                swichToFragment2();
                break;
            case R.id.main_btn3:
                swichToFragment3();
                break;
            case R.id.main_btn4:
                swichToFragment4();
                break;
        }
    }

    @Override
    public void swichToFragment1() {
/*            //태그로
            Fragment f = getSupportFragmentManager().findFragmentByTag(TAG);
            if(f==null){
                FragmentTransaction ff = getSupportFragmentManager().beginTransaction();
                ff.replace(R.id.container,new Fragment1(),TAG);
                ff.commit();
            }*/
            getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE); //저장된 backStack을 다 지움
            FragmentTransaction ft =getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, new Fragment1());
            ft.commit();
    }

    @Override
    public void swichToFragment2() {
            getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, new Fragment2());
            ft.commit();
    }

    @Override
    public void swichToFragment3() {
            getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, new Fragment3());
            ft.commit();

    }

    @Override
    public void swichToFragment4() {
                getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container, new Fragment4());
                ft.commit();

    }

    @Override
    public void swichToFragment1_1() {
            //값전달 activitiy->fragment
            Fragment1_1 m1_1 =new Fragment1_1();
            Bundle b= new Bundle();
            b.putString("name","omg");
            b.putInt("age",40);
            m1_1.setArguments(b);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, m1_1);
            ft.addToBackStack(null);
            ft.commit();

    }

    @Override
    public void swichToFragment1_2() {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, new Fragment1_2());
            ft.addToBackStack(null);
            ft.commit();
    }

    //다른 fragment에서 값을 받을 메소드
    @Override
    public void sendResult(int value) {
        Toast.makeText(this,String.valueOf(value),Toast.LENGTH_SHORT).show();
    }

    //다른 fragment에서 값을 받을 메소드
    public void mData(String a){
        Toast.makeText(this,a+":"+abc,Toast.LENGTH_SHORT).show();
    }

}
